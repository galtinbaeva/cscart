<?php /* Smarty version Smarty-3.1.21, created on 2015-07-01 16:02:33
         compiled from "C:\xampp\htdocs\cscart\design\themes\responsive\templates\addons\rus_payments\hooks\orders\tabs.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:29715593e4e98e6442-06243190%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3811d874ce7bfe08902e02b698435bc307efd973' => 
    array (
      0 => 'C:\\xampp\\htdocs\\cscart\\design\\themes\\responsive\\templates\\addons\\rus_payments\\hooks\\orders\\tabs.post.tpl',
      1 => 1434966904,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '29715593e4e98e6442-06243190',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'order_info' => 0,
    'sbrf_settings' => 0,
    'selected_section' => 0,
    'url_qr_code' => 0,
    'account_settings' => 0,
    'payment_info' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5593e4e9c08273_30147388',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5593e4e9c08273_30147388')) {function content_5593e4e9c08273_30147388($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include 'C:/xampp/htdocs/cscart/app/functions/smarty_plugins\\function.set_id.php';
?><?php
fn_preload_lang_vars(array('sbrf_recepient','sbrf_inn','sbrf_kpp','sbrf_okato_code','sbrf_settlement_account','sbrf_bank','sbrf_bik','sbrf_cor_account','sbrf_kbk','sbrf_payment','sbrf_qr_info','sbrf_information_not_found','addons.rus_payments.company_info','addons.rus_payments.organization_customer','address','phone','addons.rus_payments.account_kpp','inn_customer','addons.rus_payments.account_current','addons.rus_payments.account_personal','addons.rus_payments.account_bank','addons.rus_payments.account_bik','addons.rus_payments.account_cor','customer_information','addons.rus_payments.organization_customer','inn_customer','address','zip_postal_code','phone','addons.rus_payments.bank_details','addons.rus_payments.account_information_not_found','sbrf_recepient','sbrf_inn','sbrf_kpp','sbrf_okato_code','sbrf_settlement_account','sbrf_bank','sbrf_bik','sbrf_cor_account','sbrf_kbk','sbrf_payment','sbrf_qr_info','sbrf_information_not_found','addons.rus_payments.company_info','addons.rus_payments.organization_customer','address','phone','addons.rus_payments.account_kpp','inn_customer','addons.rus_payments.account_current','addons.rus_payments.account_personal','addons.rus_payments.account_bank','addons.rus_payments.account_bik','addons.rus_payments.account_cor','customer_information','addons.rus_payments.organization_customer','inn_customer','address','zip_postal_code','phone','addons.rus_payments.bank_details','addons.rus_payments.account_information_not_found'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['processor_params']) {?>
    <?php if ($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['processor_params']['sbrf_enabled']) {?>
        <?php $_smarty_tpl->tpl_vars["sbrf_settings"] = new Smarty_variable($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['processor_params'], null, 0);?>
        <?php if ($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_enabled']=="Y") {?>
            <div id="content_payment_information" class="<?php if ($_smarty_tpl->tpl_vars['selected_section']->value!="payment_information") {?>hidden<?php }?>">
                    <div class="sbrf">
                        <table class="ty-table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_recepient");?>
</td>
                                        <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_recepient_name'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                                    </tr>
                                    <tr>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_inn");?>
</td>
                                        <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_inn'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                                    </tr>
                                    <tr>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_kpp");?>
</td>
                                        <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_kpp'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                                    </tr>
                                    <tr>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_okato_code");?>
</td>
                                        <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_okato_code'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                                    </tr>
                                    <tr>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_settlement_account");?>
</td>
                                        <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_settlement_account'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                                    </tr>
                                    <tr>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_bank");?>
</td>
                                        <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_bank'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                                    </tr>
                                    <tr>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_bik");?>
</td>
                                        <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_bik'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                                    </tr>
                                    <tr>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_cor_account");?>
</td>
                                        <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_cor_account'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                                    </tr>
                                    <tr>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_kbk");?>
</td>
                                        <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_kbk'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                                    </tr>
                                    <tr>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_payment");?>
</td>
                                        <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_prefix'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
 №<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['order_id'], ENT_QUOTES, 'UTF-8');?>
</td>
                                    </tr>
                                    <tr>
                                        <td class="ty-left"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_qr_code']->value, ENT_QUOTES, 'UTF-8');?>
" alt="QrCode" width="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_qr_print_size'], ENT_QUOTES, 'UTF-8');?>
" height="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_qr_print_size'], ENT_QUOTES, 'UTF-8');?>
" /></td>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_qr_info");?>
</td>
                                    </tr>
                                </tbody>
                        </table>
                    </div>
            </div>
        <?php } else { ?>
            <div id="content_payment_information" class="<?php if ($_smarty_tpl->tpl_vars['selected_section']->value!="payment_information") {?>hidden<?php }?>">
                <p class="ty-no-items"><?php echo $_smarty_tpl->__("sbrf_information_not_found");?>
</p>
            </div>
        <?php }?>
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['processor_params']['account_enabled']) {?>
        <?php $_smarty_tpl->tpl_vars["account_settings"] = new Smarty_variable($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['processor_params'], null, 0);?>
        <?php $_smarty_tpl->tpl_vars["payment_info"] = new Smarty_variable($_smarty_tpl->tpl_vars['order_info']->value['payment_info'], null, 0);?>
        <?php if ($_smarty_tpl->tpl_vars['account_settings']->value['account_enabled']=="Y") {?>
            <div id="content_payment_information" class="<?php if ($_smarty_tpl->tpl_vars['selected_section']->value!="payment_information") {?>hidden<?php }?>">
                <div class="account">
                    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>__("addons.rus_payments.company_info")), 0);?>

                    <table class="ty-table">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("addons.rus_payments.organization_customer");?>
</td>
                                <td class="ty-left" width="57%"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['account_settings']->value['account_recepient_name'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("address");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['account_settings']->value['account_address'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("phone");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['account_settings']->value['account_phone'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("addons.rus_payments.account_kpp");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['account_settings']->value['account_kpp'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("inn_customer");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['account_settings']->value['account_inn'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("addons.rus_payments.account_current");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['account_settings']->value['account_current'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("addons.rus_payments.account_personal");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['account_settings']->value['account_personal'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("addons.rus_payments.account_bank");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['account_settings']->value['account_bank'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("addons.rus_payments.account_bik");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['account_settings']->value['account_bik'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("addons.rus_payments.account_cor");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['account_settings']->value['account_cor'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                        </tbody>
                    </table><br />
                    <?php if ($_smarty_tpl->tpl_vars['payment_info']->value) {?>
                    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>__("customer_information")), 0);?>

                    <table class="ty-table">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("addons.rus_payments.organization_customer");?>
</td>
                                <td class="ty-left" width="57%"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['payment_info']->value['organization_customer'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("inn_customer");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['payment_info']->value['inn_customer'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("address");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['payment_info']->value['address'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("zip_postal_code");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['payment_info']->value['zip_postal_code'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("phone");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['payment_info']->value['phone'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("addons.rus_payments.bank_details");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['payment_info']->value['bank_details'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                        </tbody>
                    </table>
                    <?php }?>
                </div>
            </div>
        <?php } else { ?>
            <div id="content_payment_information" class="<?php if ($_smarty_tpl->tpl_vars['selected_section']->value!="payment_information") {?>hidden<?php }?>">
                <p class="ty-no-items"><?php echo $_smarty_tpl->__("addons.rus_payments.account_information_not_found");?>
</p>
            </div>
        <?php }?>
    <?php }?>
<?php }?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/rus_payments/hooks/orders/tabs.post.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/rus_payments/hooks/orders/tabs.post.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['processor_params']) {?>
    <?php if ($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['processor_params']['sbrf_enabled']) {?>
        <?php $_smarty_tpl->tpl_vars["sbrf_settings"] = new Smarty_variable($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['processor_params'], null, 0);?>
        <?php if ($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_enabled']=="Y") {?>
            <div id="content_payment_information" class="<?php if ($_smarty_tpl->tpl_vars['selected_section']->value!="payment_information") {?>hidden<?php }?>">
                    <div class="sbrf">
                        <table class="ty-table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_recepient");?>
</td>
                                        <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_recepient_name'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                                    </tr>
                                    <tr>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_inn");?>
</td>
                                        <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_inn'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                                    </tr>
                                    <tr>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_kpp");?>
</td>
                                        <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_kpp'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                                    </tr>
                                    <tr>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_okato_code");?>
</td>
                                        <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_okato_code'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                                    </tr>
                                    <tr>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_settlement_account");?>
</td>
                                        <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_settlement_account'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                                    </tr>
                                    <tr>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_bank");?>
</td>
                                        <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_bank'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                                    </tr>
                                    <tr>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_bik");?>
</td>
                                        <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_bik'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                                    </tr>
                                    <tr>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_cor_account");?>
</td>
                                        <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_cor_account'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                                    </tr>
                                    <tr>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_kbk");?>
</td>
                                        <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_kbk'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                                    </tr>
                                    <tr>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_payment");?>
</td>
                                        <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_prefix'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
 №<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['order_id'], ENT_QUOTES, 'UTF-8');?>
</td>
                                    </tr>
                                    <tr>
                                        <td class="ty-left"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_qr_code']->value, ENT_QUOTES, 'UTF-8');?>
" alt="QrCode" width="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_qr_print_size'], ENT_QUOTES, 'UTF-8');?>
" height="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sbrf_settings']->value['sbrf_qr_print_size'], ENT_QUOTES, 'UTF-8');?>
" /></td>
                                        <td class="ty-left"><?php echo $_smarty_tpl->__("sbrf_qr_info");?>
</td>
                                    </tr>
                                </tbody>
                        </table>
                    </div>
            </div>
        <?php } else { ?>
            <div id="content_payment_information" class="<?php if ($_smarty_tpl->tpl_vars['selected_section']->value!="payment_information") {?>hidden<?php }?>">
                <p class="ty-no-items"><?php echo $_smarty_tpl->__("sbrf_information_not_found");?>
</p>
            </div>
        <?php }?>
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['processor_params']['account_enabled']) {?>
        <?php $_smarty_tpl->tpl_vars["account_settings"] = new Smarty_variable($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['processor_params'], null, 0);?>
        <?php $_smarty_tpl->tpl_vars["payment_info"] = new Smarty_variable($_smarty_tpl->tpl_vars['order_info']->value['payment_info'], null, 0);?>
        <?php if ($_smarty_tpl->tpl_vars['account_settings']->value['account_enabled']=="Y") {?>
            <div id="content_payment_information" class="<?php if ($_smarty_tpl->tpl_vars['selected_section']->value!="payment_information") {?>hidden<?php }?>">
                <div class="account">
                    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>__("addons.rus_payments.company_info")), 0);?>

                    <table class="ty-table">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("addons.rus_payments.organization_customer");?>
</td>
                                <td class="ty-left" width="57%"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['account_settings']->value['account_recepient_name'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("address");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['account_settings']->value['account_address'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("phone");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['account_settings']->value['account_phone'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("addons.rus_payments.account_kpp");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['account_settings']->value['account_kpp'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("inn_customer");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['account_settings']->value['account_inn'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("addons.rus_payments.account_current");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['account_settings']->value['account_current'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("addons.rus_payments.account_personal");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['account_settings']->value['account_personal'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("addons.rus_payments.account_bank");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['account_settings']->value['account_bank'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("addons.rus_payments.account_bik");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['account_settings']->value['account_bik'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("addons.rus_payments.account_cor");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['account_settings']->value['account_cor'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                        </tbody>
                    </table><br />
                    <?php if ($_smarty_tpl->tpl_vars['payment_info']->value) {?>
                    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>__("customer_information")), 0);?>

                    <table class="ty-table">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("addons.rus_payments.organization_customer");?>
</td>
                                <td class="ty-left" width="57%"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['payment_info']->value['organization_customer'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("inn_customer");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['payment_info']->value['inn_customer'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("address");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['payment_info']->value['address'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("zip_postal_code");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['payment_info']->value['zip_postal_code'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("phone");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['payment_info']->value['phone'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                            <tr>
                                <td class="ty-left"><?php echo $_smarty_tpl->__("addons.rus_payments.bank_details");?>
</td>
                                <td class="ty-left"><?php echo htmlspecialchars(htmlspecialchars_decode($_smarty_tpl->tpl_vars['payment_info']->value['bank_details'], ENT_QUOTES), ENT_QUOTES, 'UTF-8');?>
</td>
                            </tr>
                        </tbody>
                    </table>
                    <?php }?>
                </div>
            </div>
        <?php } else { ?>
            <div id="content_payment_information" class="<?php if ($_smarty_tpl->tpl_vars['selected_section']->value!="payment_information") {?>hidden<?php }?>">
                <p class="ty-no-items"><?php echo $_smarty_tpl->__("addons.rus_payments.account_information_not_found");?>
</p>
            </div>
        <?php }?>
    <?php }?>
<?php }?>
<?php }?><?php }} ?>
