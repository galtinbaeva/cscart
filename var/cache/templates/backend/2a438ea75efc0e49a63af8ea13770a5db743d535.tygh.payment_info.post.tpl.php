<?php /* Smarty version Smarty-3.1.21, created on 2015-06-30 17:39:05
         compiled from "C:\xampp\htdocs\cscart\design\backend\templates\addons\rus_payments\hooks\orders\payment_info.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:221545592aa09a5ba17-18934084%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2a438ea75efc0e49a63af8ea13770a5db743d535' => 
    array (
      0 => 'C:\\xampp\\htdocs\\cscart\\design\\backend\\templates\\addons\\rus_payments\\hooks\\orders\\payment_info.post.tpl',
      1 => 1433948811,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '221545592aa09a5ba17-18934084',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'order_info' => 0,
    'pdata' => 0,
    'show_refund' => 0,
    'pinfo' => 0,
    'primary_currency' => 0,
    'currencies' => 0,
    'processor_script' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5592aa09b21b84_15791930',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5592aa09b21b84_15791930')) {function content_5592aa09b21b84_15791930($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('addons.rus_payments.refund','addons.rus_payments.refund','addons.rus_payments.amount','addons.rus_payments.cause','cancel','refund','send','print_invoice','send','print_invoice'));
?>
<?php $_smarty_tpl->tpl_vars['pdata'] = new Smarty_variable($_smarty_tpl->tpl_vars['order_info']->value['payment_method'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['pinfo'] = new Smarty_variable($_smarty_tpl->tpl_vars['order_info']->value['payment_info'], null, 0);?>

<?php $_smarty_tpl->tpl_vars['show_refund'] = new Smarty_variable(false, null, 0);?>

<?php if ($_smarty_tpl->tpl_vars['pdata']->value['processor']=='Yandex.Money') {?>
    <?php $_smarty_tpl->tpl_vars['show_refund'] = new Smarty_variable($_smarty_tpl->tpl_vars['show_refund']->value||$_smarty_tpl->tpl_vars['pdata']->value['processor_params']['returns_enabled']=='Y'&&($_smarty_tpl->tpl_vars['pinfo']->value['yandex_confirmed_time']||!$_smarty_tpl->tpl_vars['pinfo']->value['yandex_postponed_payment'])&&!$_smarty_tpl->tpl_vars['pinfo']->value['yandex_canceled_time']&&!$_smarty_tpl->tpl_vars['pinfo']->value['yandex_refunded_time'], null, 0);?>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['pdata']->value['processor']=='Avangard') {?>
    <?php $_smarty_tpl->tpl_vars['show_refund'] = new Smarty_variable($_smarty_tpl->tpl_vars['show_refund']->value||!$_smarty_tpl->tpl_vars['pinfo']->value['avangard_canceled_time']&&!$_smarty_tpl->tpl_vars['pinfo']->value['avangard_refunded_time']&&$_smarty_tpl->tpl_vars['pinfo']->value['avangard_ticket'], null, 0);?>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['show_refund']->value) {?>
    <div class="btn-group">
        <a class="btn cm-dialog-opener cm-dialog-auto-size" data-ca-target-id="rus_payments_refund_dialog"><?php echo $_smarty_tpl->__("addons.rus_payments.refund");?>
</a>
    </div>
    <div class="hidden" title="<?php echo $_smarty_tpl->__("addons.rus_payments.refund");?>
" id="rus_payments_refund_dialog">
        <form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" class="rus-payments-refund-form cm-form-dialog-closer" name="refund_form">
            <input type="hidden" name="refund_data[order_id]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['order_id'], ENT_QUOTES, 'UTF-8');?>
" />
            <div class="control-group">
                <label class="control-label" for="rus_payments_refund_amount"><?php echo $_smarty_tpl->__("addons.rus_payments.amount");?>
 (<?php echo $_smarty_tpl->tpl_vars['currencies']->value[$_smarty_tpl->tpl_vars['primary_currency']->value]['symbol'];?>
)</label>
                <div class="controls">
                    <input type="text" name="refund_data[amount]" id="rus_payments_refund_amount" class="input-small" value="<?php echo htmlspecialchars(fn_format_price((($tmp = @$_smarty_tpl->tpl_vars['order_info']->value['total'])===null||$tmp==='' ? "0.00" : $tmp),$_smarty_tpl->tpl_vars['primary_currency']->value,null,false), ENT_QUOTES, 'UTF-8');?>
" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="rus_payments_refund_cause"><?php echo $_smarty_tpl->__("addons.rus_payments.cause");?>
</label>
                <div class="controls">
                    <textarea name="refund_data[cause]" cols="55" rows="3" id="rus_payments_refund_cause"></textarea>
                </div>
            </div>
            <div class="buttons-container">
                <a class="cm-dialog-closer cm-cancel tool-link btn"><?php echo $_smarty_tpl->__("cancel");?>
</a>
                <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>__("refund"),'but_meta'=>'','but_name'=>"dispatch[orders.rus_payments_refund]",'but_role'=>"button_main"), 0);?>

            </div>
        </form>
    <!--rus_payments_refund_dialog--></div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['processor_script']->value=='sbrf.php') {?>
    <div class="btn-group">
        <a class="btn-small cm-ajax" href="<?php echo htmlspecialchars(fn_url("orders.send_sbrf_receipt?order_id=".((string)$_smarty_tpl->tpl_vars['order_info']->value['order_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("send");?>
</a>
        <a class="btn-small cm-new-window" href="<?php echo htmlspecialchars(fn_url("orders.print_sbrf_receipt?order_id=".((string)$_smarty_tpl->tpl_vars['order_info']->value['order_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("print_invoice");?>
</a>
    </div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['processor_script']->value=='account.php') {?>
    <div class="btn-group">
        <a class="btn-small cm-ajax" href="<?php echo htmlspecialchars(fn_url("orders.send_account_payment?order_id=".((string)$_smarty_tpl->tpl_vars['order_info']->value['order_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("send");?>
</a>
        <a class="btn-small cm-new-window" href="<?php echo htmlspecialchars(fn_url("orders.print_invoice_payment?order_id=".((string)$_smarty_tpl->tpl_vars['order_info']->value['order_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("print_invoice");?>
</a>
    </div>
<?php }?><?php }} ?>
