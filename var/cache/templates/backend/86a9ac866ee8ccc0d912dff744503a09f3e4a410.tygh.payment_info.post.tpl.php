<?php /* Smarty version Smarty-3.1.21, created on 2015-06-30 17:39:05
         compiled from "C:\xampp\htdocs\cscart\design\backend\templates\addons\yandex_market\hooks\orders\payment_info.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:309655592aa0992ecc3-51598073%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '86a9ac866ee8ccc0d912dff744503a09f3e4a410' => 
    array (
      0 => 'C:\\xampp\\htdocs\\cscart\\design\\backend\\templates\\addons\\yandex_market\\hooks\\orders\\payment_info.post.tpl',
      1 => 1433948811,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '309655592aa0992ecc3-51598073',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'order_info' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5592aa099917a8_16604825',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5592aa099917a8_16604825')) {function content_5592aa099917a8_16604825($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('method','yandex_market','order_id','payment_type','yml_payment_type_','payment_method','yml_payment_method_','status','reason','yml_substatus_'));
?>
<?php if ($_smarty_tpl->tpl_vars['order_info']->value['yandex_market']) {?>

    <div class="control-group">
        <div class="control-label"><?php echo $_smarty_tpl->__("method");?>
</div>
        <div id="tygh_payment_info" class="controls"><?php echo $_smarty_tpl->__("yandex_market");?>
</div>
    </div>

    <div class="control-group">
        <div class="control-label"><?php echo $_smarty_tpl->__("order_id");?>
</div>
        <div class="controls"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['yandex_market']['order_id'], ENT_QUOTES, 'UTF-8');?>
</div>
    </div>

    <div class="control-group">
        <div class="control-label"><?php echo $_smarty_tpl->__("payment_type");?>
</div>
        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['yandex_market']['payment_type']) {?>
            <div class="controls"><?php ob_start();
echo htmlspecialchars(strtolower($_smarty_tpl->tpl_vars['order_info']->value['yandex_market']['payment_type']), ENT_QUOTES, 'UTF-8');
$_tmp1=ob_get_clean();?><?php echo $_smarty_tpl->__("yml_payment_type_".$_tmp1);?>
</div>
        <?php }?>
    </div>

    <div class="control-group">
        <div class="control-label"><?php echo $_smarty_tpl->__("payment_method");?>
</div>
        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['yandex_market']['payment_method']) {?>
            <div class="controls"><?php ob_start();
echo htmlspecialchars(strtolower($_smarty_tpl->tpl_vars['order_info']->value['yandex_market']['payment_method']), ENT_QUOTES, 'UTF-8');
$_tmp2=ob_get_clean();?><?php echo $_smarty_tpl->__("yml_payment_method_".$_tmp2);?>
</div>
        <?php }?>
    </div>

    <?php if ($_smarty_tpl->tpl_vars['order_info']->value['yandex_market']['status']) {?>
        <div class="control-group">
            <div class="control-label"><?php echo $_smarty_tpl->__("status");?>
</div>
            <div class="controls"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['yandex_market']['status'], ENT_QUOTES, 'UTF-8');?>
</div>
        </div>
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['order_info']->value['yandex_market']['substatus']) {?>
        <div class="control-group">
            <div class="control-label"><?php echo $_smarty_tpl->__("reason");?>
</div>
            <div class="controls"><?php ob_start();
echo htmlspecialchars(strtolower($_smarty_tpl->tpl_vars['order_info']->value['yandex_market']['substatus']), ENT_QUOTES, 'UTF-8');
$_tmp3=ob_get_clean();?><?php echo $_smarty_tpl->__("yml_substatus_".$_tmp3);?>
</div>
        </div>
    <?php }?>

<?php }?><?php }} ?>
