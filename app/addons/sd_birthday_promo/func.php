<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Mailer;

function fn_sd_send_birthday_promo_notifications()
{
    $result = false;
    $this_year = date('Y', TIME);
    $users_data = db_get_array("SELECT user_id, birthday, email, lang_code FROM ?:users");

    foreach ($users_data as $user_data) {
        if (!empty($user_data['birthday'])) {
            $last_year_birth = date('Y', $user_data['birthday']);
            if ($last_year_birth < $this_year) {
                $notify = Registry::get('addons.sd_birthday_promo.notify') * SECONDS_IN_DAY;  
                
                if (!empty($notify)) {
                    $time_action = fn_sd_get_time_action_promo($user_data['user_id'], $user_data['birthday']);
                    $warn_to = $time_action['from'] - $notify;
                    $warn_from = $warn_to - SECONDS_IN_DAY;
                    if ($warn_from < TIME && TIME < $warn_to) {
                        $res = Mailer::sendMail(array(
                            'to' => $user_data['email'],
                            'from' => 'company_orders_department',
                            'data' => array(),
                            'tpl' => 'addons/sd_birthday_promo/mail_warning_birthday.tpl',
                        ), 'C', $user_data['lang_code']);
                        $result == false ? $result = array() : $result;
                        $result[$user_data['user_id']] = $user_data['email'];
                    }
                }
            }
        }
    }
    
    return $result;
}

function fn_sd_get_birthday(&$cart, &$auth)
{
    $result = false;
    
    if ($auth['user_id'] > 0) {
        $last_applied = db_get_row('SELECT timestamp, last_birthday FROM ?:birthday_promo_log WHERE user_id = ?s AND is_applied = ?s 
        ORDER BY id DESC LIMIT 1', $auth['user_id'], 'Y'); 
        if (empty($last_applied)) {
            $last_applied['last_birthday'] = null;
        }
        
        $time_action = fn_sd_get_time_action_promo($auth['user_id'], $last_applied['last_birthday']);
        if (empty($last_applied['last_birthday']) || $last_applied['last_birthday'] < $time_action['from']) {
            if ($time_action['to'] >= TIME && $time_action['from'] <= TIME) {
                $result = true;
            }
        }
    }
    
    return $result;
}

function fn_sd_get_time_action_promo($user_id, $date_birth)
{
    if (empty($date_birth)) {
        $date_birth = db_get_field('SELECT birthday FROM ?:users WHERE user_id = ?s', $user_id);
    }
    $birthday_month = date('m',$date_birth);
    $birthday_day = date('d',$date_birth);
    $from = mktime(0, 0, 0, $birthday_month, $birthday_day);
    
    $promotion_duration = Registry::get('addons.sd_birthday_promo.promotion_duration');
    if (empty($promotion_duration)) {
        $promotion_duration = 1;
    }
    $promotion_duration = $promotion_duration * SECONDS_IN_DAY;
    
    $to = $from + $promotion_duration;
    $time_action = array(
        'from' => $from,
        'to' => $to
    );
    return $time_action;
}

/* 
 * Hooks
 */
 

function fn_sd_birthday_promo_change_order_status($status_to, $status_from, $order_info, $force_notification, $order_statuses, $place_order)
{   
    $result = false;   
    foreach ($order_info['promotions'] as $id_promo => $content_promo) {
        $promo_data = fn_get_promotion_data($id_promo);
        if ($promo_data['conditions']['conditions'][1]['condition'] == 'birthday') {
            $result = true;
            break;
        }
    }
    
    if ($result) {
        $order_id = $order_info['order_id'];
        $user_id = $order_info['user_id'];
        $promo_order_info = db_get_row('SELECT order_id, last_birthday FROM ?:birthday_promo_log WHERE user_id = ?s AND order_id=?s ORDER BY order_id DESC LIMIT 1', $user_id, $order_id );     
        if (empty($promo_order_info)) {
            $last_birthday = db_get_field("SELECT birthday FROM ?:users WHERE user_id = ?s", $user_id);
        } else {
            $last_birthday = $promo_order_info['last_birthday'];
        }
        
        if (empty($promo_order_info) || $promo_order_info['order_id'] != $order_id) {
                $time_action = fn_sd_get_time_action_promo($user_id, $last_birthday);
                $insert_db = array(
                    'user_id' => $user_id,
                    'timestamp' => TIME,
                    'order_id' => $order_id,
                    'is_applied' => 'Y',
                    'last_birthday' => $time_action['from']
                );
                db_query("INSERT INTO ?:birthday_promo_log ?e", $insert_db);
                
            } elseif (in_array($status_to, fn_get_order_paid_statuses()) && !in_array($status_from, fn_get_order_paid_statuses())) {
                $update_status = array(
                    'is_applied' => 'Y'
                );
                db_query('UPDATE ?:birthday_promo_log SET ?u WHERE order_id = ?s', $update_status, $order_id);
                
            } elseif (!in_array($status_to, fn_get_order_paid_statuses()) && in_array($status_from, fn_get_order_paid_statuses())) {
                $update_status = array(
                    'is_applied' => 'N'
                );
            db_query('UPDATE ?:birthday_promo_log SET ?u WHERE order_id = ?s', $update_status, $order_id);
        }
    } 
}

/*
 * \Hooks
 */