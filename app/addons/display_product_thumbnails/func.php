<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_display_product_thumbnails_get_order_info(&$order, $additional_data) {

foreach ($order['products'] as $k => $v){
$order['products'][$k]['main_pair'] = fn_get_image_pairs($order['products'][$k]['product_id'], 'product', 'M'); 
//$order['product_groups']['0']['products'][$k]['main_pair'];
}
//fn_print_r($order['product_groups']['0']['products']['1630258919']['main_pair']);
}