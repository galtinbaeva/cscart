<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

function fn_order_prefix_get_data($order_id)
{
fn_set_notification('W',1,print_r($order_id, true));
    $data = db_get_field("SELECT order_number_pref FROM ?:order_details WHERE order_id = ?i", $order_id);
	return $data;
}

function fn_order_prefix_set_data($order_id, $order_number_pref)
{

    $set_delimiter = ';';
    $pair_delimiter = ':';
    $left = '[';
    $right = ']';

    $data = json_decode($order_number_pref, true);

    if (is_array($data)) {
        $data = serialize($data);
        $insert = array(
            'order_id' => $order_id,
            'order_number_pref' => $data
        );

        db_query("REPLACE INTO ?:order_details ?e", $insert);
    }

    return true;
}