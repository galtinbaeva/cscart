<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

$schema['references'] = array(
        'order_details' => array(
            'reference_fields' => array('order_id' => '#key'),
            'join_type' => 'LEFT'
        )
	);

$schema['export_fields']['Order prefix'] = array(
'table' => 'order_details',
'db_field' => 'order_number_pref',
//'multilang' => true,
//'process_get' => array('fn_order_prefix_get_data','#this'),
//'process_put' => array('fn_order_prefix_set_data','#this')

);

return $schema;
